@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('ユーザー情報') }}</div>

                <div class="card-body">

                    <div><img src="/uploads/user/{{ $user->img }}" width="200px" height="200px" class="rounded-circle"></div>
                    <div>{{ $user->id }}</div>
                    <div>{{ $user->name }}</div>
                    <div>{{ $user->email }}</div>

                    <form method="GET" action="{{ route('user.edit', ['id' => $user->id]) }}">
                    @csrf
                        <div class="form-group row">
                          <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">編集する</button>
                          </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
