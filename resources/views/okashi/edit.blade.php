@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">
        <div class="card-header">
        組み合わせ詳細 編集
        <a href="{{ route('okashi.show', ['id' => $okashi->id ]) }}" class="btn btn-secondary">戻る</a>
        </div>
        <div class="card-body">
          <form method="POST" action="{{ route('okashi.update', [ 'id' => $okashi->id ]) }}" enctype="multipart/form-data">
          @csrf

            <div class="form-group row">
              <label for="name1" class="col-md-4 col-form-label text-md-right">名前１</label>
              <div class="col-md-6">
                <input id="name1" type="text" class="form-control" name="name1" value="{{ $okashi->name1 }}">
              </div>
            </div>

            <div class="form-group row">
              <label for="img1" class="col-md-4 col-form-label text-md-right">写真１</label>
              <div class="col-md-6">
                <input id="img1" type="file" class="form-control" name="img1" value="{{ $okashi->img1 }}">
                <!-- 画像プレビュー -->
                <img id="preview-img1" src="/uploads/{{ $okashi->img1 }}" width="200px" height="200px"></div>
              </div>
            </div>

            <div class="form-group row">
              <label for="name2" class="col-md-4 col-form-label text-md-right">名前２</label>
              <div class="col-md-6">
                <input id="name2" type="text" class="form-control" name="name2" value="{{ $okashi->name2 }}">
              </div>
            </div>

            <div class="form-group row">
              <label for="img2" class="col-md-4 col-form-label text-md-right">写真２</label>
              <div class="col-md-6">
                <input id="img2" type="file" class="form-control" name="img2" value="{{ $okashi->img2 }}">
                <!-- 画像プレビュー -->
                <img id="preview-img2" src="/uploads/{{ $okashi->img2 }}" width="200px" height="200px"></div>
              </div>
            </div>

            <div class="form-group row">
              <label for="taste" class="col-md-4 col-form-label text-md-right">味</label>
              <div class="col-md-6">
                <!-- <input id="taste" type="text" class="form-control" name="taste"> -->
                <select name="taste" class="form-control">
                  <option value="5">絶品</option>
                  <option value="4">かなりおいしい</option>
                  <option value="3" selected>おいしい</option>
                  <option value="2">そんなに</option>
                  <option value="1">まずい</option>
                </select>
              </div>
            </div>

            <div class="form-group row">
              <label for="price" class="col-md-4 col-form-label text-md-right">値段</label>
              <div class="col-md-6">
                <!-- <input id="price" type="text" class="form-control" name="price"> -->
                <select name="price" class="form-control">
                  <option value="5">激安</option>
                  <option value="4">安い</option>
                  <option value="3" selected>お手頃</option>
                  <option value="2">ちょい高</option>
                  <option value="1">高い</option>
                </select>
              </div>
            </div>

            <div class="form-group row">
              <label for="surprise" class="col-md-4 col-form-label text-md-right">驚き</label>
              <div class="col-md-6">
                <!-- <input id="surprise" type="text" class="form-control" name="surprise"> -->
                <select name="surprise" class="form-control">
                  <option value="5">予想外</option>
                  <option value="4">意外</option>
                  <option value="3" selected>ちょい意外</option>
                  <option value="2">まぁある</option>
                  <option value="1">ど定番</option>
                </select>
              </div>
            </div>

            <div class="form-group row">
              <label for="comment" class="col-md-4 col-form-label text-md-right">コメント</label>
              <div class="col-md-6">
                <textarea class="form-control" name="comment">{{ $okashi->comment }}</textarea>
              </div>
            </div>

            <div class="form-group row">
              <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-primary">更新する</button>
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
