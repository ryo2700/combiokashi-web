@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-10">
      <div class="card">
        <div class="card-header">
          マイページ
          <a href="{{ route('okashi.create') }}" class="btn btn-primary">新規登録</a>
          </div>
        <div class="card-body">
          <table class="table">
            <thead>
              <tr>
                <th scope="id">id</th>
                <th scope="name1">名前１</th>
                <th scope="name2">名前２</th>
                <th scope="taste">味</th>
                <th scope="col">値段</th>
                <th scope="col">驚き</th>
                <th scope="col">コメント</th>
                <th scope="col">詳細</th>
                <th scope="col">name</th>
              </tr>
            </thead>
            <tbody>
              @foreach($myOkashis as $okashi)
                <tr>
                  <div>
                    <td>{{ $okashi->id }}</td>
                    <td>
                      <img src="/uploads/{{ $okashi->img1 }}" width="100px" height="100px">
                      <div class="text-center">{{ $okashi->name1 }}</div>
                    </td>
                    <td>
                      <img src="/uploads/{{ $okashi->img2 }}" width="100px" height="100px">
                      <div class="text-center">{{ $okashi->name2 }}</div>
                    </td>
                    <td>{{ $okashi->starEva($okashi->taste) }}</td>
                    <td>{{ $okashi->starEva($okashi->price) }}</td>
                    <td>{{ $okashi->starEva($okashi->surprise) }}</td>
                    <td>{{ $okashi->comment }}</td>
                    <td><a href="{{ route('okashi.show', [ 'id' => $okashi->id ]) }}">詳細を見る</a></td>
                    <td>{{ $okashi->user->name }}</td>
                  </div>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
