@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">
        <div class="card-header">詳細</div>
        <div class="card-body">
          <div>
            <div>{{ $okashi->id }}</div>
            <span>
              <img src="/uploads/{{ $okashi->img1 }}" width="200px" height="200px">
              {{ $okashi->name1 }}
            </span>
            <span>
              <img src="/uploads/{{ $okashi->img2 }}" width="200px" height="200px">
              {{ $okashi->name2 }}
            </span>
          </div>
          <div>
            <span>{{ $okashi->starEva($okashi->taste) }}</span>
            <span>{{ $okashi->starEva($okashi->price) }}</span>
            <span>{{ $okashi->starEva($okashi->surprise) }}</span>
          </div>
          <div>
            <span>{{ $okashi->comment }}</span>
          </div>
            <div class="row"><img src="/uploads/user/{{ $okashi->user->img }}" width="50px" height="50px" class="rounded-circle"></div>

          @if ($okashi->user_id == Auth::id())
            <form method="GET" action="{{ route('okashi.edit', [ 'id' => $okashi->id ]) }}">
            @csrf
                <div class="form-group row">
                  <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary">編集する</button>
                  </div>
                </div>
            </form>

            <form method="POST" action="{{ route('okashi.destroy', [ 'id' => $okashi->id ]) }}" id="delete_{{ $okashi->id }}">
            @csrf
              <div class="form-group row">
                <div class="col-md-6 offset-md-4">
                  <a href="#" class="btn btn-danger" data-id="{{ $okashi->id }}" onclick="deletePost(this);">削除する</a>
                </div>
              </div>
            </form>
          @else
          @endif

        </div>
      </div>
    </div>
  </div>
</div>

<script>
/********************************
 * 削除ボタンを押してすぐにレコードが削除
 * されるのも問題なので、一旦javascriptで
 * 確認メッセージを流します。
 *********************************/
function deletePost(e) {
  'use strict';
  if (confirm('本当に削除していいですか？')) {
    document.getElementById('delete_' + e.dataset.id).submit();
  }
}
</script>

@endsection
