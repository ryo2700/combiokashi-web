@extends('layouts.app')

@section('content')
<div class="container">

  <div id="regist-okashi" class="mb-2 pb-1 d-flex align-items-end justify-content-center">
    <a href="{{ route('okashi.create') }}" class="btn btn-success btn-lg">
      <i class="fas fa-coffee pr-2"></i>おかしセット投稿
    </a>
  </div>

  <div class="row justify-content-center">

    <div id="new-post" class="col-md-8">
      <div class="card">
        <div class="card-header">最新投稿</div>

          <div class="card-body">
              @foreach($okashis as $okashi)
                  <div class="card">
                    <div class="card-body">
                    <div class="row">
                      <div class="col-md-6 row">
                        <div class="col-6">
                          <!-- <img src="/uploads/{{ $okashi->img1 }}" class="w-100 h-100"> -->
                          <img src="/uploads/{{ $okashi->img1 }}" class="w-100 h-75">
                          <div class="text-center">{{ $okashi->name1 }}</div>
                        </div>
                        <div class="col-6">
                          <img src="/uploads/{{ $okashi->img2 }}" class="w-100 h-75">
                          <div class="text-center">{{ $okashi->name2 }}</div>
                        </div>
                      </div>
                      <div class="col-md-6">
                          <div class="row">
                            <div class="col-4">
                             <div class="text-center">味</div>
                             <div>{{ $okashi->starEva($okashi->taste) }}</div>
                            </div>
                            <div class="col-4">
                              <div class="text-center">価格</div>
                              <div>{{ $okashi->starEva($okashi->price) }}</div>
                            </div>
                            <div class="col-4">
                              <div class="text-center">驚き</div>
                              <div>{{ $okashi->starEva($okashi->surprise) }}</div>
                            </div>
                          </div>
                          <div class="row">
                            <!-- <div class="card w-100"> -->
                              <div class="">{{ $okashi->comment }}</div>
                            <!-- </div> -->
                          </div>
                          <div class="row d-flex align-items-end justify-content-end">
                            <a href="{{ route('okashi.show', [ 'id' => $okashi->id ]) }}">詳細</a>
                            {{ $okashi->updated_at->format('n/j G:i') }}
                            <img src="/uploads/user/{{ $okashi->user->img }}" width="50px" height="50px" class="rounded-circle">
                          </div>
                      </div>
                    </div>
                    </div>
                  </div>
              @endforeach
          </div>
      </div>
    </div>

    <div id="total-lanking-post" class="col-md-4">
      <div class="card">
        <div class="card-header">総合 ランキングBest5</div>
        <div class="card-body">
        @foreach($totalRankOkashis as $okashi)
        <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-2">
              <div class="row">
                @if ($loop->index == 0)
                  <i class="fas fa-crown fa-2x fa-gold"></i>
                @elseif ($loop->index == 1)
                  <i class="fas fa-crown fa-2x fa-gray"></i>
                @elseif ($loop->index == 2)
                  <i class="fas fa-crown fa-2x fa-copper"></i>
                @else
                  <i class="fas fa-arrow-right fa-2x"></i>
                @endif
              </div>
              <div class="row"><h4>{{ $okashi->taste + $okashi->price + $okashi->surprise}}</h4> Pt</div>
              <div class="row"><a href="{{ route('okashi.show', [ 'id' => $okashi->id ]) }}">詳細</a></div>
              <div class="row"><img src="/uploads/user/{{ $okashi->user->img }}" width="30px" height="30px" class="rounded-circle"></div>
            </div>
            <div class="col-5">
              <img src="/uploads/{{ $okashi->img1 }}" class="w-75 h-75">
              <div class="text-center">{{ $okashi->name1 }}</div>
            </div>
            <div class="col-5">
              <img src="/uploads/{{ $okashi->img2 }}" class="w-75 h-75">
              <div class="text-center">{{ $okashi->name2 }}</div>
            </div>
          </div>
        </div>
        </div>
        @endforeach
        </div>
      </div>
    </div>

  </div>
</div>
@endsection
