
//画像プレビュー表示
$('#img1').on('change', function(e) {
  var reader = new FileReader();
  reader.onload = function(e) {
    $("#preview-img1").attr('src', e.target.result);
  }
  reader.readAsDataURL(e.target.files[0]);
});

//画像プレビュー表示
$('#img2').on('change', function(e) {
  var reader = new FileReader();
  reader.onload = function(e) {
    $("#preview-img2").attr('src', e.target.result);
  }
  reader.readAsDataURL(e.target.files[0]);
});

//画像プレビュー表示
$('#user_img').on('change', function(e) {
  var reader = new FileReader();
  reader.onload = function(e) {
    $("#preview-user_img").attr('src', e.target.result);
  }
  reader.readAsDataURL(e.target.files[0]);
});
