<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Okashi;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
//use App\Services\IconStar;

class OkashiController extends Controller
{
    public function index() {

        //$okashis = Okashi::all();
        $okashis = Okashi::orderby('updated_at', 'desc')->get();

        $totalRankOkashis = Okashi::orderby('taste', 'desc')
                            ->orderby('price', 'desc')
                            ->orderby('surprise', 'desc')
                            ->limit(5)
                            ->get();
        //$okashis = DB::table('okashis')
        //->select('id', 'name1', 'name2', 'taste', 'price', 'surprise', 'comment')
        //->orderby('id', 'desc')
        //->get();
        //dd($okashis);

        return view('okashi.index', compact('okashis', 'totalRankOkashis'));
        //return view('okashi.index', compact('okashis', 'taste'));
    }

    public function create() {
        return view('okashi.create');
    }

    public function store(Request $request) {
        $okashi = new Okashi;

        $okashi->name1 = $request->input('name1');
        //$okashi->img1 = $request->input('img1');
        $okashi->name2 = $request->input('name2');
        //$okashi->img2 = $request->input('img2');
        $okashi->taste = $request->input('taste');
        $okashi->price = $request->input('price');
        $okashi->surprise = $request->input('surprise');
        $okashi->comment = $request->input('comment');
        $okashi->user_id = Auth::id();

        if ($file = $request->img1) {
            $fileName1 = time() . $file->getClientOriginalName();
            $targetPath = public_path('uploads/');
            $file->move($targetPath, $fileName1);
        } else {
            $fileName1 = "";
        }
        $okashi->img1 = $fileName1;

        if ($file = $request->img2) {
            $fileName2 = time() . $file->getClientOriginalName();
            $targetPath = public_path('uploads/');
            $file->move($targetPath, $fileName2);
        } else {
            $fileName2 = "";
        }
        $okashi->img2 = $fileName2;

        $okashi->save();
        //return redirect('okashi/index');
        return redirect('okashi/mypage');
    }

    public function show($id) {
        $okashi = Okashi::find($id);
        //dd($okashi);
        return view('okashi.show', compact('okashi'));
    }

    public function edit($id) {
        $okashi = Okashi::find($id);
        return view('okashi.edit', compact('okashi'));
    }

    public function update(Request $request, $id) {
        $okashi = Okashi::find($id);

        $okashi->name1 = $request->input('name1');
        //$okashi->img1 = $request->input('img1');
        $okashi->name2 = $request->input('name2');
        //$okashi->img2 = $request->input('img2');
        $okashi->taste = $request->input('taste');
        $okashi->price = $request->input('price');
        $okashi->surprise = $request->input('surprise');
        $okashi->comment = $request->input('comment');
        $okashi->user_id = Auth::id();

        if ($file = $request->img1) {
            $fileName1 = $file->getClientOriginalName();
            $targetPath = public_path('uploads/');
            $file->move($targetPath, $fileName1);
            $okashi->img1 = $fileName1;
        } else {
            //$fileName1 = "";
        }
        //$okashi->img1 = $fileName1;

        if ($file = $request->img2) {
            $fileName2 = $file->getClientOriginalName();
            $targetPath = public_path('uploads/');
            $file->move($targetPath, $fileName2);
            $okashi->img2 = $fileName2;
        } else {
            //$fileName2 = "";
        }
        //$okashi->img2 = $fileName2;

        $okashi->save();
        //return redirect('okashi/show/{id}', compact('okashi'));
        return view('okashi.show', compact('okashi'));
    }

    public function destroy($id) {
        $okashi = Okashi::find($id);
        $okashi->delete();

        return redirect('okashi/index');
    }

    public function mypage() {
        //$myOkashis = Okashi::all()->where('user_id', Auth::id());
        $myOkashis = Okashi::orderby('updated_at', 'desc')->where('user_id', Auth::id())->get();
        return view('okashi.mypage', compact('myOkashis'));
    }
}
