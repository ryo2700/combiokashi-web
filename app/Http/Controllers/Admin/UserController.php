<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;

class UserController extends Controller
{
    //
    public function show() {
        $user = Auth::user();
        return view('user.show', compact('user'));
    }

    public function edit($id) {
        $user = User::find($id);
        return view('user.edit', compact('user'));
    }

    public function update(Request $request, $id) {
        $user = User::find($id);

        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->updated_at = now();

        if ($file = $request->img) {
            $fileName = $file->getClientOriginalName();
            $targetPath = public_path('uploads/user/');
            $file->move($targetPath, $fileName);
            $user->img = $fileName;
        } else {
            //$fileName = "no-image.png";
        }
        //$user->img = $fileName;
        //dd($user);
        $user->save();
        return view('user.show', compact('user'));
    }
}
