<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Okashi extends Model
{
    //
  public function starEva($eva) {
    for ($i = 0; $i < 5; $i++) {
      if ($eva > $i) {
        echo '<i class="fas fa-star"></i>';
      } else {
        echo '<i class="far fa-star"></i>';
      }
    }
  }

  public function user() {
      return $this->belongsTo('App\User');
  }

}
