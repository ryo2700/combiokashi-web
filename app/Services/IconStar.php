<?php

namespace App\Services;

class IconStar {
  public static function starEva($eva) {
    for ($i = 0; $i <0; $i++) {
      if ($eva > $i) {
        echo '<i class="fa fa-star"></i>';
      } else {
        echo '<i class="fa fa-star-o"></i>';
      }
    }
  }
}
