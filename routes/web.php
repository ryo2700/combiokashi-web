<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/', 'OkashiController@index');

//ログインしていないと、index,showしか見れないように
Route::group(['prefix' => 'okashi', 'middleware' => 'auth'], function(){
    //Route::get('index', 'OkashiController@index')->name('okashi.index');
    Route::get('create', 'OkashiController@create')->name('okashi.create');
    Route::post('store', 'OkashiController@store')->name('okashi.store');
    //Route::get('show/{id}', 'OkashiController@show')->name('okashi.show');
    Route::get('edit/{id}', 'OkashiController@edit')->name('okashi.edit');
    Route::post('update/{id}', 'OkashiController@update')->name('okashi.update');
    Route::post('destroy/{id}', 'OkashiController@destroy')->name('okashi.destroy');

    //マイページはAuth関数で制御できるからgetで。
    Route::get('mypage', 'OkashiController@mypage')->name('okashi.mypage');
});

//ログインしていなくても、index,showは見れるように
Route::group(['prefix' => 'okashi'], function(){
    Route::get('index', 'OkashiController@index')->name('okashi.index');
    Route::get('show/{id}', 'OkashiController@show')->name('okashi.show');
});

Route::group(['prefix' => 'user', 'middleware' => 'auth'], function(){
    Route::get('show', 'Admin\UserController@show')->name('user.show');
    Route::get('edit/{id}', 'Admin\UserController@edit')->name('user.edit');
    Route::post('update/{id}', 'Admin\UserController@update')->name('user.update');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
